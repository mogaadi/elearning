package com.bezkoder.springjwt.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.springjwt.models.course;
public interface courseRepository extends JpaRepository<course, Long>{
	  List<course> findByPublished(boolean published);
	  List<course> findByTitleContaining(String title);
}

package com.bezkoder.springjwt.models;

public enum ERole {
  ROLE_ETUDIANT,
  ROLE_FORMATEUR,
  ROLE_ADMIN,
  ROLE_SUPERADMIN,
  ROLE_AGENCE
}

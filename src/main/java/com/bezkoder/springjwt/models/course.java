package com.bezkoder.springjwt.models;
import javax.persistence.*;
@Entity
@Table(name = "course")
public class course {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idcourse;
	@Column(name = "title")
	private String title;
	@Column(name = "description")
	private String description;
	@Column(name = "published")
	private boolean published;
	
	
	public course() {
		
	}
	public course(String title, String description, boolean published) {
		super();
		this.title = title;
		this.description = description;
		this.published = published;
	}
	public long getIdcourse() {
		return idcourse;
	}
	public void setIdcourse(long idcourse) {
		this.idcourse = idcourse;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isPublished() {
		return published;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	
	@Override
	public String toString() {
		return "course [idcourse=" + idcourse + ", title=" + title + ", desc=" + description + ", published=" + published + "]";
	}
}

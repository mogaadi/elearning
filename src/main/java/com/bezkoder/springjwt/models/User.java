package com.bezkoder.springjwt.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users", 
    uniqueConstraints = { 
      @UniqueConstraint(columnNames = "username"),
      @UniqueConstraint(columnNames = "email"),
      @UniqueConstraint(columnNames = "adresse"),
      @UniqueConstraint(columnNames = "telephone") 
    })
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long iduser;

  @NotBlank
  @Size(max = 20)
  private String username;

  @NotBlank
  @Size(max = 50)
  @Email
  private String email;

  @NotBlank
  @Size(max = 120)
  private String adresse;
  
  private int telephone;
  
  @NotBlank
  @Size(max = 120)
  
  private String password;

  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(  name = "user_roles", 
        joinColumns = @JoinColumn(name = "user_id"), 
        inverseJoinColumns = @JoinColumn(name = "role_id"))
  private Set<Role> roles = new HashSet<>();

 




public User(Long iduser, String username, String email,
		 String adresse, int telephone,
		 String password, Set<Role> roles) {
	super();
	this.iduser = iduser;
	this.username = username;
	this.email = email;
	this.adresse = adresse;
	this.telephone = telephone;
	this.password = password;
	this.roles = roles;
}



public User() {
	super();
}



public User(String username,String email,
		String adresse,int telephone,
		 String password, Set<Role> roles) {
	super();
	this.username = username;
	this.email = email;
	this.adresse = adresse;
	this.telephone = telephone;
	this.password = password;
	this.roles = roles;
}




public User(String username,String email,
		String adresse,int telephone,
		 String password) {
	super();
	this.username = username;
	this.email = email;
	this.adresse = adresse;
	this.telephone = telephone;
	this.password = password;
}



public Long getIduser() {
	return iduser;
}

public void setIduser(Long iduser) {
	this.iduser = iduser;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getAdresse() {
	return adresse;
}

public void setAdresse(String adresse) {
	this.adresse = adresse;
}

public int getTelephone() {
	return telephone;
}

public void setTelephone(int telephone) {
	this.telephone = telephone;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public Set<Role> getRoles() {
	return roles;
}

public void setRoles(Set<Role> roles) {
	this.roles = roles;
}


}

package com.bezkoder.springjwt.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bezkoder.springjwt.models.ERole;
import com.bezkoder.springjwt.models.Role;
import com.bezkoder.springjwt.models.User;
import com.bezkoder.springjwt.payload.request.AddRequest;
import com.bezkoder.springjwt.payload.request.LoginRequest;
import com.bezkoder.springjwt.payload.response.JwtResponse;
import com.bezkoder.springjwt.payload.response.MessageResponse;
import com.bezkoder.springjwt.repository.RoleRepository;
import com.bezkoder.springjwt.repository.UserRepository;
import com.bezkoder.springjwt.security.jwt.JwtUtils;
import com.bezkoder.springjwt.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/signin")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();    
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt, 
                         userDetails.getIduser(), 
                         userDetails.getUsername(), 
                         userDetails.getEmail(), 
                         roles));
  }

  @PostMapping("/adduser")
  public ResponseEntity<?> addUser(@Valid @RequestBody AddRequest addRequest) {
    if (userRepository.existsByUsername(addRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username is already taken!"));
    }

    if (userRepository.existsByEmail(addRequest.getEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email is already in use!"));
    }
    
    if (userRepository.existsByTelephone(addRequest.getTelephone())) {
        return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: number is already in use!"));
      }
 
    // Create new user's account
    User user = new User(addRequest.getUsername(), addRequest.getEmail(), 
    		addRequest.getAdresse(), 
    		addRequest.getTelephone(), 
    		encoder.encode(addRequest.getPassword()));

    Set<String> strRoles = addRequest.getRole();
    Set<Role> roles = new HashSet<>();

    if (strRoles == null) {
      Role userRole = roleRepository.findByName(ERole.ROLE_ETUDIANT)
          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
      roles.add(userRole);
    } else {
      strRoles.forEach(role -> {
        switch (role) {
        case "admin":
          Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(adminRole);

          break;
        case "formateur":
          Role formateurRole = roleRepository.findByName(ERole.ROLE_FORMATEUR)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(formateurRole);

          break;
          
        case "agence":
            Role agence = roleRepository.findByName(ERole.ROLE_AGENCE)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(agence);

            break;
            
        case "superadmin":
            Role superadmin = roleRepository.findByName(ERole.ROLE_SUPERADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(superadmin);

            break;
        default:
          Role userRole = roleRepository.findByName(ERole.ROLE_ETUDIANT)
              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
          roles.add(userRole);
        }
      });
    }
    

   user.setRoles(roles);
    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    
  }
  
//update user's account
  @PutMapping(value="/ModifierUser/{iduser}")
	public ResponseEntity<?> ModifierUser(@PathVariable("iduser")Long id ,@Valid @RequestBody AddRequest addRequest ) {
		if (userRepository.existsByUsername(addRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(addRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}
		// Create new user's account
/*		    User user = new User(addRequest.getUsername(), addRequest.getEmail(), 
    		addRequest.getAdresse(), 
    		addRequest.getTelephone(), 
    		encoder.encode(addRequest.getPassword()));*/
		
		Set<String> strRoles = addRequest.getRole();
		Set<Role> roles = new HashSet<>();
		User user2 = userRepository.findById(id).get();
		user2.setUsername(addRequest.getUsername());
		user2.setEmail(addRequest.getEmail());
		encoder.encode(addRequest.getPassword());
		if (strRoles == null) {
		      Role userRole = roleRepository.findByName(ERole.ROLE_ETUDIANT)
		          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		      roles.add(userRole);
		    } else {
		      strRoles.forEach(role -> {
		        switch (role) {
		        case "admin":
		          Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
		              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		          roles.add(adminRole);

		          break;
		        case "formateur":
		          Role formateurRole = roleRepository.findByName(ERole.ROLE_FORMATEUR)
		              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		          roles.add(formateurRole);

		          break;
		          
		        case "agence":
		            Role agence = roleRepository.findByName(ERole.ROLE_AGENCE)
		                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		            roles.add(agence);

		            break;
		            
		        case "superadmin":
		            Role superadmin = roleRepository.findByName(ERole.ROLE_SUPERADMIN)
		                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		            roles.add(superadmin);

		            break;
		        default:
		          Role userRole = roleRepository.findByName(ERole.ROLE_ETUDIANT)
		              .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		          roles.add(userRole);
				}
			});
		}

		user2.setRoles(roles);
		userRepository.save(user2);

		return ResponseEntity.ok(new MessageResponse("User Modifier successfully!"));
	}
  
  

	@DeleteMapping("/deleteuser/{idU}")
	@ResponseBody
	public void deleteUser(@PathVariable("idU") long id) {
		
		User u = userRepository.findById(id).get();
		u.setRoles(null);
		userRepository.save(u);	
		userRepository.deleteById(id);
	}
  
}
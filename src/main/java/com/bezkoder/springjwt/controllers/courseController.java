package com.bezkoder.springjwt.controllers;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.bezkoder.springjwt.models.course;
import com.bezkoder.springjwt.repository.courseRepository;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api/test")
public class courseController {
	 @Autowired
	 courseRepository courseRepository;
	 
	 
	  @GetMapping("/course")
	  public ResponseEntity<List<course>> getAllcourse(@RequestParam(required = false) String title) {
	    try {
	      List<course> course = new ArrayList<course>();
	      if (title == null)
	    	  courseRepository.findAll().forEach(course::add);
	      else
	    	  courseRepository.findByTitleContaining(title).forEach(course::add);
	      if (course.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }
	      return new ResponseEntity<>(course, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	  @GetMapping("/course/{idcourse}")
	  public ResponseEntity<course> getcourseById(@PathVariable("idcourse") long id) {
	    Optional<course> courseData = courseRepository.findById(id);
	    if (courseData.isPresent()) {
	      return new ResponseEntity<>(courseData.get(), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	  @PostMapping("/ajoutercourse")
	  public ResponseEntity<course> createTutorial(@RequestBody course course) {
	    try {
	    	 courseRepository
	          .save(new course(course.getTitle(), course.getDescription(), false));
	      return new ResponseEntity<>(course, HttpStatus.CREATED);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	  @PutMapping("/course/{id}")
	  public ResponseEntity<course> updatecourse(@PathVariable("id") long id, @RequestBody course course) {
	    Optional<course> courseData = courseRepository.findById(id);
	    if (courseData.isPresent()) {
	    	course course1 = courseData.get();
	    	course1.setTitle(course1.getTitle());
	    	course1.setDescription(course1.getDescription());
	    	course1.setPublished(course1.isPublished());
	      return new ResponseEntity<>(courseRepository.save(course1), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	  @DeleteMapping("/course/{idcourse}")
	  public ResponseEntity<HttpStatus> deletecourse(@PathVariable("id") long id) {
	    try {
	    	courseRepository.deleteById(id);
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	  @DeleteMapping("/course")
	  public ResponseEntity<HttpStatus> deleteAllcourse() {
	    try {
	    	courseRepository.deleteAll();
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	  @GetMapping("/course/published")
	  public ResponseEntity<List<course>> findByPublished() {
	    try {
	      List<course> course = courseRepository.findByPublished(true);
	      if (course.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }
	      return new ResponseEntity<>(course, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }

}
